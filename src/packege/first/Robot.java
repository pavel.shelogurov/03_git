package packege.first;

public class Robot {

    private String name;
    private String type;
    
    public Robot (String name, String type) {
	this.name = name;
	this.type = type;
    }
    
    
    public String getName () {
	return name;
    }
    
    public String getType () {
	return type;
    }
    
    public void info () {
	System.out.println("I'm robot");
	System.out.println("My name is "+name);
	System.out.println("My type is "+type);

    }
}
